WITH WeeklySales AS (
    SELECT
        s.time_id,
        calendar_week_number,
        day_name,
        SUM(amount_sold) AS weekly_amount
    FROM
        sh.sales s
    JOIN
        sh.times t ON s.time_id = t.time_id
    WHERE
        EXTRACT(WEEK FROM s.time_id) IN (49, 50, 51)
        AND EXTRACT(YEAR FROM s.time_id) = 1999
    GROUP BY
        s.time_id, calendar_week_number, day_name
),

CenteredMovingAvg AS (
    SELECT
        time_id,
        LAG(weekly_amount) OVER (ORDER BY time_id) AS prev_weekly_amount,
        weekly_amount AS current_weekly_amount,
        LEAD(weekly_amount) OVER (ORDER BY time_id) AS next_weekly_amount
    FROM
        WeeklySales
)

SELECT
    W.calendar_week_number,
	W.time_id,
    W.day_name,
    W.weekly_amount AS sales,
    W.weekly_amount + COALESCE(LAG(W.weekly_amount) OVER (ORDER BY W.time_id), 0) AS cum_sum,
    ROUND(
        (C.prev_weekly_amount + C.current_weekly_amount + C.next_weekly_amount) / 3,
        2
    ) AS centered_3_day_avg
FROM
    WeeklySales W
JOIN
    CenteredMovingAvg C ON W.time_id = C.time_id
ORDER BY
    W.time_id;
